#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>

#include <EventLoop/Worker.h>
#include <caloAnalysis/analysis.h>
#include <AsgTools/MessageCheck.h>

#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/Message.h"
#include "xAODJet/JetContainer.h"
#include "xAODEventInfo/EventInfo.h"

#include "xAODJet/JetAuxContainer.h"
#include <fastjet/PseudoJet.hh>
#include <TFile.h>

#include <SampleHandler/MetaDataSample.h>
#include "xAODCaloEvent/CaloClusterContainer.h"

// this is needed to distribute the algorithm to the workers
ClassImp(analysis)



analysis :: analysis ()
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}



EL::StatusCode analysis :: setupJob (EL::Job& job)
{
  ANA_CHECK_SET_TYPE (EL::StatusCode);
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  job.useXAOD ();
  ANA_CHECK_SET_TYPE (EL::StatusCode); // set type of return code you are expecting (add to top of each function once)                             
  ANA_CHECK(xAOD::Init());

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode analysis :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  TFile *outputFile = wk()->getOutputFile (outputName);
  OutTree = new TTree ("OutTree", "OutTree");
  OutTree->SetDirectory(outputFile);


  return EL::StatusCode::SUCCESS;
}



EL::StatusCode analysis :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode analysis :: changeInput (bool firstFile)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode analysis :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.
  ANA_CHECK_SET_TYPE (EL::StatusCode); // set type of return code you are expecting (add to top of each function once)                             
  
  xAOD::TEvent* event = wk()->xaodEvent();
  m_store = wk()->xaodStore();

  Info("initialize()", "Number of events = %lli", event->getEntries() );


  return EL::StatusCode::SUCCESS;
}



EL::StatusCode analysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  ANA_CHECK_SET_TYPE (EL::StatusCode); // set type of return code you are expecting (add to top of each function once)

  xAOD::TEvent* event = wk()->xaodEvent();


   const xAOD::EventInfo* eventInfo = 0;
  ANA_CHECK(event->retrieve( eventInfo, "EventInfo"));
  double lumi = wk()->metaData()->castDouble("lumi");

  /*
  std::cout << "the luminosity is: " << lumi << std::endl;
  std::cout << "actualInteractionsPerCrossing: " << eventInfo->actualInteractionsPerCrossing () << std::endl;
  std::cout << "averageInteractionsPerCrossing: " << eventInfo->averageInteractionsPerCrossing () << std::endl;
  */

  const xAOD::CaloClusterContainer* TopoClusters;
  const xAOD::JetContainer* antikt4jets; 
  const xAOD::JetContainer* antikt10jets;
  const xAOD::JetContainer* AntiKt4TruthJets;
  const xAOD::JetContainer* AntiKt10TruthJets;

  ANA_CHECK( event->retrieve( AntiKt10TruthJets,"AntiKt10TruthJets" ));
  ANA_CHECK( event->retrieve( AntiKt4TruthJets, "AntiKt4TruthJets" ));
  ANA_CHECK( event->retrieve( TopoClusters, "CaloCalTopoClusters" ));
  ANA_CHECK( event->retrieve( antikt4jets, "AntiKt4LCTopoJets" ));
  ANA_CHECK( event->retrieve( antikt10jets, "AntiKt10LCTopoJets" ));

  std::cout << "total number of clusters: " << TopoClusters->size() << std::endl; 
  std::cout << "total number of akt 4 jets: " << antikt4jets->size() << std::endl;
  std::cout << "leading jet pt: " << antikt4jets->at(0)->pt() << std::endl;
  std::cout << "************************************************* " << AntiKt4TruthJets->size() << std::endl;

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode analysis :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode analysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode analysis :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  return EL::StatusCode::SUCCESS;
}
