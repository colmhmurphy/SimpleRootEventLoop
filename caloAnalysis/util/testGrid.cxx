#include "caloAnalysis/analysis.h"

#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "SampleHandler/DiskListLocal.h"
#include <TSystem.h>
#include "SampleHandler/Sample.h"
#include <SampleHandler/MetaDataSample.h>

#include "EventLoopGrid/PrunDriver.h"

#include <EventLoopAlgs/NTupleSvc.h>
#include <EventLoop/OutputStream.h>

int main( int argc, char* argv[] ) {

  // Take the submit directory from the input if provided:                                                                
  std::string submitDir = "submitDir";
  if( argc > 1 ) submitDir = argv[ 1 ];

  // Set up the job for xAOD access:                                                                                      
  xAOD::Init().ignore();


  // Construct the samples to run on:                                                                                     
  SH::SampleHandler sh;

  SH::scanRucio (sh, "user.avaidya.JZ5W_e1996_s2630_s2183_r7768_mu140test2_EXT0" );

  // Set the name of the input TTree. It's always "CollectionTree"                                                                                     
  // for xAOD files.                                                                                                                                   
  sh.setMetaString( "nc_tree", "CollectionTree" );

  // Print what we found:                                                                                                                              
  sh.print();

  // Create an EventLoop job:                                                                                                                          
  EL::Job job;
  job.sampleHandler( sh );
  job.options()->setDouble (EL::Job::optMaxEvents, -1);

  //Add the output stream for the tree                                                                                                                \
                                                                                                                                                       
  EL::OutputStream output  ("myOutput");
  job.outputAdd (output);
  EL::NTupleSvc *ntuple = new EL::NTupleSvc ("myOutput");
  job.algsAdd (ntuple);

  // Add our analysis to the job:                                                                                                                      
  // MyxAODAnalysis* alg = new MyxAODAnalysis();                                                                                                       
  analysis* alg = new analysis();
  //jetsStudy* alg = new jetsStudy();                                                                                                                  

  job.algsAdd( alg );
  alg->outputName = "myOutput"; // give the name of the output to our algorithm                                                                        

  //Run the job using the local/direct driver:                                                                                                         
  EL::PrunDriver driver;
  driver.options()->setString("nc_outputSampleName", "user.avaidya.SigmaMu140.%in:name[3]%.");


  driver.submit( job, submitDir );

  return 0;
}
